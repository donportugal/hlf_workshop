docker kill $(docker ps -q) &&
docker rm $(docker ps -aq) &&
docker rmi $(docker images | grep 'dev')
docker network prune

rm -rf /tmp/fabric*
