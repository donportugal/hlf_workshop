jq --version > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "Please Install 'jq' https://stedolan.github.io/jq/ to execute this script"
	echo
	exit 1
fi
starttime=$(date +%s)

################## ENROLL USER #####################
echo "POST request Enroll on Org1  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=Expressif&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo $ORG1_TOKEN


################# authentication ###################

  ### push ###

  transactionID=$(echo ESP00001)
  timestamp=$(echo "2018-11-30_16:40:00")
  hashedMSG=$(echo "457de643c3113667f18660bf12c999db721a3fc4")
  deviceType=$(echo "ESP8266")
  version=$(echo "2.0")

  echo
  TRX_ID=$(curl -s -X POST \
    http://localhost:4000/channels/mychannel/chaincodes/mycc \
    -H "authorization: Bearer $ORG1_TOKEN" \
    -H "content-type: application/json" \
    -d '{
    "fcn":"push",
    "args":["'$transactionID'","'$timestamp'","'$hashedMSG'","'$deviceType'","'$version'"]
  }')
  echo "Transacton ID is $TRX_ID"
  echo
